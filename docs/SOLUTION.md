## Problem

A Chocolate Factory is trying a new kind of chocolate bar making process that
involves continuously pouring hot chocolate over cold walls that move through a
conveyor belt, and then solidify and create dynamic shapes for the bar. The excess
chocolate is then recaptured and filled into the new walls.

Implement a calculator that receives the heights of the walls as they are formed,
and computes the of amount of solid chocolate so far. It is expected for this
calculator to run continuously (there's a clear beginning, but not clear end), and
we would like to query the calculator at any time, to check the current amount of
solidified chocolate.

##### Example:
```
@ = unit of liquid
|*| = wall

time = t1:
Sequence of wall heights: [0 2 0 1]

    |*|
|*|@|*|_
 1 0 2 0
This contains 1 unit of chocolate


|*|@ @|*|
|*|*|@|*|_
 2 1 0 2 0
This contains three units of chocolate.

time = t3 (t2 after receiving the sequence of heights 0, 1, 0, 3, 0:

 |*|
 |*|@ @ @|*|@ @|*|
_|*|@|*|@|*|*|@|*|_
0 3 0 1 0 2 1 0 2 0
This contains 8 units of chocolate.
```

#### First step

Implement the calculator that receives the next wall height and that also allows
for querying the current amount of solidified chocolate.

#### Second step
Provide a REST Api to receive a new wall, and provide the current solid chocolate
amount

#### Third step
Inside the project's documentation, answer the following question: What's the
space/time complexities of the solution you implemented?

This system will be run 24h a day and the fabric does not stop to take break (poor
chocolate workers), so the system will be running for a very long time always
receiving new walls and recomputing the amount of chocolate. Take this into account
when providing your solution.

## Solution
### First step

To solve this problem, multiple attemps have been made.

First, I've tried to calculate the amount of chocolate in a single pass, by storing it in a "possible_chocolate" var and once I found a wall that was able to keep that chocolate, save the value. However, this would not work for cases were the left "big wall" was higher than the right big wall (or vice-versa).

Then, I've tried to come up with a solution were I would stack all walls and unstack them once I started filling them. This was tricky and I couldn't actually find a solution doind this.

Later, I decided to step back and think in a more simple way: The amount of chocolate that a single space in the conveyor belt can hold is the same amount that the biggest walls around it can hold. This was the first solution I had that would work for all possibilities. However, it has a time complexity of O(N^2), since for each part of the belt I would have to check for all other parts to it's right and to it's left for the bigger neighbours

Then I decided to optimize it by precalculating the bigger walls to the left and to the right of each part of the conveyor belt. This would allow me to have a time complexity of O(N), since I would be doing constant time lookups for each part of the belt, and the preprocessing would also need only constant time operations. This, however, added a space complexity of O(N), since I would need to store two more arrays for each part of the conveyour belt

Finally, I optimized the solution to store the biggest walls on variables and check them on the go. A more in depth complexity analisys can be found in Third Step section.

The non-final solutions can be found at [src/old](../src/old)

### Second step

The first step to create a web API was to think exatcly what I wanted to serve. My main idea was to create an RESTful API with a colletion of ConveyorBelt resources that would have an amount of chocolate based on the walls provided to it. So you could send a POST to `/conveyorbelts` to create a new conveyor belt, and then send a POST to `/conveyorbelts/1` with a list of walls to add them to this conveyor belt, and finally send a GET to `/conveyorbelts/1` to get the total amount of solidified chocolate in that wall.

#### Api DevLog

In this section I tried to summarize all the choices I made for this project. The paragraphs describes the choices I made in a almost chronological order.

##### Design choices

I've evaluated a few choices before starting to develop the web API. At first, I've considered using Django. However, a whole Django project felt too cumbersome for a simple API.

Then, I considered Flask. However, I would like to have a few extras to this API such as having OpenAPI documentation to the endpoint and maybe having some sort of ORM to handle database models. This would require installing a few extensions. I wanted something with more "batteries included".

Finally, I decided to go with FastAPI. The choice was basically because I wanted to try it out, since it uses async operations in Python. However, it also uses type hinting to automatically validate request data and generate API documentation using OpenAPI specification, which are useful features for this API.

Since I decided to go with FastAPI, I would need to use external tools for ORM and migrations. For ORM, I used SQLAlchemy since it was recommended in the oficial FastAPI docs. For handling migrations with SQLAlchemy, Alembic was the best choice. Also, to keep the project simple, I decided to use SQLite for database.

To handle the project dependencies and manage virtual environments, I decided to go with Poetry. It can handles the dependencies and virtual envs with ease, making the process to keep this project running in different environments easy. 

Since FastAPI is based on uvicorn, which is one of the fastest servers for python, and I wanted to keep this project as simple as possible, I ended up not setting up a load balancer for the project such as Nginx.

For deploying the project, I decided to go with a simple AWS EC2 virtual machine and keep running it on port 5000, since it would take almost no effort to set up.

##### Development

First, I created a simple API that would calculate the amount of chocolate for a given wall. I used a simple pydantic model called ConveyorBelt to hold the data (which would also be used to serialize data in requests/responses), and created a method inside this model to calculate its amount of chocolate. I also created a model called Walls, which is a simple data holder for a list of strings (and is also a pydantic model, which has out of the box data validation in request handling).

Then, I started to search for a way to persist the ConveyorBelts. Using SQLAlchemy seemed like a good idea, since it is the most common ORM choice for Python. I started using it with SQLite to get things running quickly. Also, I'm using Alembic to handle migrations.

Later, I reorganized the project since the files were getting confusing.

Finally, I looked up for some deployment options, but since I was running out of time, I decided to just run this API on a bare AWS EC2 instance using SQLite. Improvements for this solution would be to use a managed database instance, such as Amazon RDS, and deploy it using docker.

### Third Step

Finally, lets analyze the time and space complexity of the final solution:

Since the biggest left wall is always calculated based on the previous current walls, O(1) on its verification and calculation.

To calculate the right wall, I run the array once to find the biggest right wall, and then I only re-run it again once I get to that biggest wall. Aside from purely descending values or from a belt were all the values are the same, we can assume that we will be running at max twice through the whole array to find the biggest right wall. 

So, in conclusion, this solutions takes O(1) space complexity and O(n) time complexity. (It does around n(1st right wall checkup) + n(2nd right wall checkups) + n(passing through each part of the conveyor belt) = 3n which has time complexity of O(n))