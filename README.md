# Chocolate Factory

Solving the chocolate factory problem.

The api is (or should be) running at http://ec2-54-94-69-19.sa-east-1.compute.amazonaws.com:5000/

You can check the api documentation at http://ec2-54-94-69-19.sa-east-1.compute.amazonaws.com:5000/docs

## Setup
This project dependencies are controlled using [Poetry](https://python-poetry.org/). Please install it to run locally.

1. Clone this repo: `git clone https://gitlab.com/rafaelmonteiro95/chocolate-factory.git`
2. Install dependencies: `poetry install`
3. Start a shell inside the virtualenv: `poetry shell`
4. Run migrations using alembic: `alembic upgrade head`
4. Run the server: `python src/app.py`
5. go to [http://localhost:5000/docs](http://localhost:5000/docs) and try the API. 


## Project Structure

Inside the [src](src/) folder you will find the main code for the API. The files are organized as follows:
* [src/app.py](./src/app.py): The main server; handles routes and calls methods for db operations
* [src/database.py](./src/database.py): Utilitary database logic
* [src/conveyor_belt.py](./src/conveyor_belt.py): Contains the ConveyorBelt model and schema. The ConveyorBeltModel has the method `add_walls()` that adds new walls to the conveyor belt and calculates the new amount of chocolate.
* [src/old/solution.py](./src/old/solution.py): Contains the solutions created in a non-API manner. Kept just to showcase the solutions attempts I made
* [src/old/test_solution.py](./src/old/test_solution.py): I used these tests to evaluate the correctness of solution.py

Inside the [docs](docs/) folder you will find the [SOLUTION.MD](docs/SOLUTION.md) doc, which describes all the steps I took build this project, and also provides the time and space complexity of this solution.