def calculate_total_chocolate1(belt):
    """
    DOES NOT WORK FOR 1 2 3 0 2 4 5 3 3 4

    The amount of chocolate in the whole factory can be
    broken into the amount of chocolate between each pair of walls.

    So, in order to calculate it, I need to find each pair of walls in which
    a previous wall is smaller or equal than a to a next wall.

    While i'm traversing the conveyor belt searching for pairs of walls,
    I'm  also calculating the possible amount of chocolate
    that such walls might hold. Once I find a pair of walls, i'll add that
    possible amount of chocolate to the total and start calculating from zero
    again.
    """
    last_wall = belt[0]
    total_chocolate = 0
    possible_chocolate = 0
    for current_wall in belt[1:]:
        # checking if found a bigger current_wall
        if current_wall >= last_wall:
            last_wall = current_wall
            if possible_chocolate > 0:
                total_chocolate += possible_chocolate
            possible_chocolate = 0

        # haven't found a new wall, calculate possible chocolate
        possible_chocolate += last_wall - current_wall

    return total_chocolate


def calculate_total_chocolate3(belt):
    """simply looks at each piece of conveyor belt, finds biggest
    possible left value, biggest possible right value,
    and fills the piece with the smaller of the two.

    Arguments:
        belt {list[int]} -- the conveyor belt

    Returns:
        int-- the total amount of chocolate filled
    """
    total_choc = 0
    for idx, part in enumerate(belt):
        # find biggest possible left wall
        biggest_left = -1
        for left_wall in reversed(belt[:idx]):
            if left_wall > biggest_left:
                biggest_left = left_wall
        # find biggest possible right wall
        biggest_right = -1
        for right_wall in belt[idx:]:
            if right_wall > biggest_right:
                biggest_right = right_wall

        new_choc = None
        if biggest_left > part and biggest_right > part:
            new_choc = min(biggest_left, biggest_right) - part

        if new_choc:
            total_choc += new_choc

    return total_choc


def calculate_total_chocolate4(belt):
    """trying to optimize the time complexity of the solution by
    pre calculating the biggest left and right values

    For example, if we have the belt as:
    [1, 2, 3, 0, 2, 4, 5, 3, 3, 4]
    the biggest walls for each part of the belt would be
    Left: [-1, 1, 2, 3, 3, 3, 4, 5, 5, 5]
    Right: [5, 5, 5, 5, 5, 4, 4, 4, 4, -1]

    Arguments:
        belt {list[int]} -- the conveyor belt

    Returns:
        int -- total amount of chocolate filled
    """
    biggest = -1
    biggest_left_walls = [biggest] * len(belt)
    biggest_right_walls = [biggest] * len(belt)

    for idx in range(len(belt)):
        biggest_left_walls[idx] = biggest
        if biggest < belt[idx]:
            biggest = belt[idx]

    biggest = -1
    for idx in reversed(range(len(belt))):
        biggest_right_walls[idx] = biggest
        if biggest < belt[idx]:
            biggest = belt[idx]

    total_choc = 0
    for idx, part in enumerate(belt):
        # find biggest possible left wall
        biggest_left = biggest_left_walls[idx]

        # find biggest possible right wall
        biggest_right = biggest_right_walls[idx]

        new_choc = None
        if biggest_left > part and biggest_right > part:
            new_choc = min(biggest_left, biggest_right) - part

        if new_choc:
            total_choc += new_choc

    return total_choc


def calculate_total_chocolate5(belt):
    """trying to optimize the space complexity of the solution by
    calculating the biggest left and right values on the go,
    and storing them in a variable.

    Arguments:
        belt {list[int]} -- the conveyor belt

    Returns:
        int -- total amount of chocolate filled
    """

    total_choc = 0
    biggest_left = -1
    biggest_right = -1
    for wall in belt:
        if wall > biggest_right:
            biggest_right = wall

    for idx, current_wall in enumerate(belt):

        new_choc = None
        if biggest_left > current_wall and biggest_right > current_wall:
            new_choc = min(biggest_left, biggest_right) - current_wall

        if new_choc:
            total_choc += new_choc

        # re-calculating stuff
        if current_wall >= biggest_right:
            biggest_right = -1
            for wall in belt[idx+1:]:
                if wall > biggest_right:
                    biggest_right = wall
        if current_wall >= biggest_left:
            biggest_left = current_wall

    return total_choc
