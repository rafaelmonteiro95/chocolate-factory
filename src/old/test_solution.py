from solution import (
    calculate_total_chocolate3,
    calculate_total_chocolate4,
    calculate_total_chocolate5
)
import pytest


@pytest.mark.parametrize("calc_func", [
        pytest.param(calculate_total_chocolate3, id='3'),
        pytest.param(calculate_total_chocolate4, id='4'),
        pytest.param(calculate_total_chocolate5, id='5'),
    ]
)
@pytest.mark.parametrize(
    "belt, expected_chocolate", [
        pytest.param([1, 0, 1], 1,
                     id='Minimal case'),
        pytest.param([0, 2, 0, 1, 2, 0, 1, 0, 3, 0], 8,
                     id="example provided"),
        pytest.param([1, 2, 3, 0, 2, 4, 5, 3, 3, 4], 6,
                     id="two slopes")
    ]
)
def test_calculate_total_chocolate(belt, expected_chocolate, calc_func):
    choc = calc_func(belt)
    assert expected_chocolate == choc
