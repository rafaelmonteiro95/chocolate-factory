from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from pydantic import BaseModel
from typing import List

from database import Base


class WallsSchema(BaseModel):
    heights: List[int]


class ConveyorBeltSchema(BaseModel):
    """Pydantic schema for data validation in requests"""
    belt_id: int
    previous_tallest_wall: int = None
    total_chocolate: int = 0


class ConveyorBeltModel(Base):
    """SQLAlchemy ORM model for database operations"""
    __tablename__ = "conveyor_belt"

    belt_id = Column(Integer, primary_key=True, index=True)
    previous_tallest_wall = Column(Integer, default=None)
    total_chocolate = Column(Integer, default=0)

    def schema(self):
        """turns a Model into its schema

        Returns:
            ConveyorBeltSchema -- The schema representing this model
        """
        return ConveyorBeltSchema(
            belt_id=self.belt_id,
            previous_tallest_wall=self.previous_tallest_wall,
            total_chocolate=self.total_chocolate
        )

    def add_walls(self, walls: WallsSchema) -> int:
        """Adds new walls to this conveyor belt and calculates
        its total chocolate

        Arguments:
            walls {WallsSchema} -- The walls to add

        Returns:
            int -- The amount of new chocolate added by these walls
        """
        extra_chocolate = 0
        # calculating the biggest next wall for the first time
        next_tallest_wall = -1
        for wall in walls.heights:
            if wall > next_tallest_wall:
                next_tallest_wall = wall

        if self.previous_tallest_wall is None:
            self.previous_tallest_wall = -1

        # calculating the amount of chocolate each wall can hold
        for idx, current_wall in enumerate(walls.heights):

            new_choc = None
            if self.previous_tallest_wall > current_wall and \
               next_tallest_wall > current_wall:
                new_choc = min(
                    self.previous_tallest_wall,
                    next_tallest_wall) - current_wall

            if new_choc:
                extra_chocolate += new_choc

            # re-calculating biggest walls
            if current_wall >= next_tallest_wall:
                next_tallest_wall = -1
                for wall in walls.heights[idx+1:]:
                    if wall > next_tallest_wall:
                        next_tallest_wall = wall
            if current_wall >= self.previous_tallest_wall:
                self.previous_tallest_wall = current_wall

        self.total_chocolate += extra_chocolate
        return extra_chocolate
