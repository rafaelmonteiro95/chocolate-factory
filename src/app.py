from typing import List

import uvicorn
from sqlalchemy.orm import Session
from fastapi import FastAPI, Depends

from database import SessionLocal, engine, Base
from conveyor_belt import WallsSchema, ConveyorBeltModel, ConveyorBeltSchema


Base.metadata.create_all(bind=engine)


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


app = FastAPI()


@app.post(
    "/conveyorbelts/",
    response_model=ConveyorBeltSchema
)
async def create_conveyor_belt(
    db: Session = Depends(get_db)
):
    # fetch conveyor belt from db
    belt = ConveyorBeltModel()
    db.add(belt)
    db.commit()
    db.refresh(belt)
    return belt.schema()


@app.get(
    "/conveyorbelts/",
    response_model=List[ConveyorBeltSchema]
)
async def list_conveyor_belts(
    db: Session = Depends(get_db)
) -> List[ConveyorBeltSchema]:
    # fetch conveyor belts from db
    belts = db.query(ConveyorBeltModel).all()
    return [belt.schema() for belt in belts]


@app.post(
    "/conveyorbelts/{conveyor_belt_id}",
    response_model=ConveyorBeltSchema
)
async def add_walls_to_conveyor_belt(
    conveyor_belt_id: int,
    walls: WallsSchema,
    db: Session = Depends(get_db)
):
    # fetch conveyor belt from db
    belt = db.query(ConveyorBeltModel).filter(
        ConveyorBeltModel.belt_id == conveyor_belt_id
    ).first()

    # updating chocolate in the belt
    belt.add_walls(walls)
    db.commit()

    return belt.schema()


@app.get(
    "/conveyorbelts/{conveyor_belt_id}",
    response_model=ConveyorBeltSchema
)
async def get_conveyor_belt_data(
    conveyor_belt_id: int,
    db: Session = Depends(get_db)
):
    # fetch conveyor belt from db
    belt = db.query(ConveyorBeltModel).filter(
        ConveyorBeltModel.belt_id == conveyor_belt_id
    ).first()

    return belt.schema()


if __name__ == "__main__":
    uvicorn.run("app:app", host="0.0.0.0", port=5000, reload=True)
